#coding=gbk
class OCR:
    def __init__(self):
        #print("请输入百度AI的OCR服务的AK和SK:")
        #self.AK=input("AK:")
        #self.SK=input("SK:")
        self.AK="WAeKMXlVKWUjmuYawL2A6aMg"
        self.SK="vQc6qAjDek2QDHG7q2dW0QlQmgTFO4XC"
        
    def _get_token(self):
        import urllib.request
        # client_id 为官网获取的AK， client_secret 为官网获取的SK
        host = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id={}&client_secret={}'.format(self.AK,self.SK)
        request = urllib.request.Request(host)
        request.add_header('Content-Type', 'application/json; charset=UTF-8')
        response = urllib.request.urlopen(request)
        content = response.read()
        content= str(content,encoding="utf-8")
        content=eval(content)
        #print(content)
        return content["access_token"]

    def _output_jpg(self,path):
        from PIL import Image
        im = Image.open(path)
        rgb_im = im.convert('RGB')
        print(path.split("\\")[-1])
        rgb_im.save(str(path.split("\\")[-1])+".jpg")
        

    def _ocr(self,path):
        import urllib.request,urllib.parse
        import base64
        from io import BytesIO
        from PIL import Image
        from io import StringIO
        im = Image.open(BytesIO(path))
        rgb_im = im.convert('RGB')
        output_buffer = BytesIO()
        rgb_im.save(output_buffer, format='JPEG')
        binary_data = output_buffer.getvalue()
        base64_data = base64.b64encode(binary_data)
        #print(type(base64_data))
        access_token = self._get_token()
        url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general?access_token=' + access_token
        #img = base64.b64encode(img)
        
        params = {"image": base64_data,"language_type":"CHN_ENG"}
        params = urllib.parse.urlencode(params).encode(encoding='UTF8')
        request = urllib.request.Request(url, params)
        request.add_header('Content-Type', 'application/x-www-form-urlencoded')
        response = urllib.request.urlopen(request)
        content = response.read()
        content= str(content,encoding="utf-8")
        content=eval(content)
        #print(content)
        return content

    def _img_size(self,path):
        from io import BytesIO
        from PIL import Image
        im = Image.open(path)
        return im.size

if __name__=="__main__":
    te=OCR()
    te._ocr(r".\之法律意见书\1")
        
