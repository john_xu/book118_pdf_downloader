class Selector:
    css_title_intop="#Preview > div.title > span > h1"
    css_title_inbot="#viewcontent"
    css_goto_iframe="#Preview > div:nth-child(4) > div > div.view-btn"
    css_btn_np="#newView > div.bar > div > a.item.page-next"
    css_btn_pp="#newView > div.bar > div > a.item.page-prev"
    css_btn_readall="#btn_read"
    css_content_img="#newView > div.bd > div:nth-child({}) > img"
    css_pg_num="#newView > div.bar > div > input"
    css_all_pg="#newView > div.bd> div"
    xpath_btn_np='//*[@id="newView"]/div[1]/div/a[5]'
    xpath_btn_pp='//*[@id="newView"]/div[1]/div/a[4]'
    css_ifame="#layer_new_view_iframe"
    css_btn_np_1="#nextBtn"
    css_btn_pp_1="#preBtn"
