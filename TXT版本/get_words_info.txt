class GetWordsInfo:

    def __init__(self,dirc):
        self.content=dirc

    def _get_log_id(self):# return a string
        return self.content["log_id"]

    def _get_words_result_num(self):# return a string
        return self.content["words_result_num"]

    def get_words_result(self):# return a list
        return self.content["words_result"]

    def _get_words_result_location(self,result):# return a dirc
        return result["location"]

    def get_words_result_words(self,result):# return a string
        return result["words"]

    def get_words_result_location_width(self,result):# return a int
        location=self._get_words_result_location(result)
        return int(location["width"])

    def get_words_result_location_top(self,result):# return a int
        location=self._get_words_result_location(result)
        return int(location["top"])

    def get_words_result_location_height(self,result):# return a int
        location=self._get_words_result_location(result)
        return int(location["height"])

    def get_words_result_location_left(self,result):# return a int
        location=self._get_words_result_location(result)
        return int(location["left"])

