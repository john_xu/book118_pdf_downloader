#coding=gbk

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selector import Selector
import re
import time
import requests
import os
from urllib import parse
from get_words_info import GetWordsInfo
from baiduai import OCR
from html_frame_writer import HtmlFrameWriter

class MySpider:
    slc=Selector()
    ocr=OCR()
    
    def __init__(self,driverhead="--headless"):
        self.file_address=self._get_file_address()
        chrome_options=Options()
        chrome_options.add_argument(driverhead)
        self.browser=webdriver.Chrome(options=chrome_options)
        self.browser.implicitly_wait(30)
        self.browser.get(self.file_address)
        
        self.file_info=self._get_file_info()
        self.title=self._get_title()
        print("{}\t\t".format(self.title),end="")
        self.fmt=self._get_format()
        print("{}\t\t".format(self.fmt),end="")
        self.page_count=self._get_page_count()
        print("{}页".format(self.page_count))
        
    def _get_file_address(self):
        print("")
        print("请粘贴(单击鼠标右键)包含文档的网址，例如'https://max.book118.com/html/2018/1127/8077054136001134.shtm'")
        address=input("-->")
        return address
    
    def _get_file_info(self):
        try:
            try:
                file_info = self.browser.find_element_by_css_selector(self.slc.css_title_intop).text
            except:
                file_info = self.browser.find_element_by_css_selector(self.slc.css_title_inbot).text
                file_info = file_info + " 0页"
            return file_info
        except:
            return "未找到文档标题"
    def _get_format(self):
        pattern="\.[a-z]{3,4} "
        fmt=re.search(pattern,self.file_info,re.IGNORECASE).group()#输出类似“.pfd ”字符串
        fmt=fmt[1:-1]
        return fmt

    def _get_title(self):
        pattern="^(\S)+\."
        title=re.search(pattern,self.file_info,re.IGNORECASE).group()#输出类似“公司债券发行之法律意见书.”字符串
        title=title[:-1]
        return title

    def _get_page_count(self):
        pattern=" [0-9]+"
        page_count_s=re.search(pattern,self.file_info,re.IGNORECASE).group()#输出类似“ 14”字符串
        page_count_i=int(page_count_s[1:])
        return page_count_i

    def _goto_iframe(self):
        self.browser.find_element_by_css_selector(self.slc.css_goto_iframe).click()
        print("已进入预览页面")
        self.browser.switch_to.frame("layer_new_view_iframe")
        print("浏览器进入frame")

    def _get_pg_num(self):
        pg_num=self.browser.find_element_by_css_selector(self.slc.css_pg_num).text
        return int(pg_num)

    def _wait_loading(self):
        time.sleep(1)
        try:
            self.self.browser.find_element_by_class("loading")
            time.sleep(0.2)
            print("wait loading")
            self._wait_loading()
        except:
            pass
    def _next_page(self):
        try:
            self.browser.find_element_by_xpath(self.slc.xpath_btn_np).click()
        except:
            self.browser.find_element_by_css_selector(self.slc.css_btn_np_1).click()
        time.sleep(0.5)

    def _pre_page(self):
        try:
            self.browser.find_element_by_xpath(self.slc.xpath_btn_pp).click()
        except:
            self.browser.find_element_by_css_selector(self.slc.css_btn_pp_1).click()
        time.sleep(0.1)

    def _read_all(self):
        print("正在浏览文件")
        pg_num=1
        while pg_num<=self.page_count:
            self._next_page()
            #self.browser.find_element_by_xpath(self.slc.xpath_btn_np).click()
            print("\r已完成\t{:.1f}%".format(pg_num/self.page_count*50),end="")
            pg_num+=1
        self._wait_loading()
        print("\r等待加载",end="")
        time.sleep(5)
        
        while pg_num>=1:
            self._pre_page()
            #self.browser.find_element_by_xpath(self.slc.xpath_btn_pp).click()
            print("\r已完成\t{:.1f}%".format(50+(1-pg_num/self.page_count)*50),end="")
            pg_num-=1
        print("\r已完成\t100.0%")

    def _get_all_page(self):
        all_page=self.browser.find_elements_by_css_selector(self.slc.css_all_pg)
        return all_page

    def _get_one_page(self,page):
        try:
            if page.get_attribute("class")=="webpreview-item":
                outerHTML=page.get_attribute("outerHTML")
                img_link=self._get_img_link(outerHTML)
                img_link=self._add_protocol(img_link)
                return img_link
            else:
                pass
        except:
            img_link=""
                  

    def _get_img_link(self,outerHTML):
        pattern='src=(\S+)png'
        img_link=re.search(pattern,outerHTML,re.IGNORECASE).group()
        img_link=img_link[5:]
        return img_link

    def _add_protocol(self,img_link):
        return parse.urljoin("https://www.taobao.com",img_link)

    def _img_downloader(self,img_link,n):
        r=requests.get(img_link)
        img=r.content
        self._creat_path()
        path=r"./{}/{}".format(self.title,n)
        with open(path,"wb") as f:
            f.write(img)
            content=self.ocr._ocr(img)
            dirc=GetWordsInfo(content)
            html_writer=HtmlFrameWriter()
            begain=html_writer.add_start(path)
            end=html_writer.add_end()
            results=dirc.get_words_result()
            frame=begain
            for result in results:
                words=dirc.get_words_result_words(result)
                width=dirc.get_words_result_location_width(result)
                top=dirc.get_words_result_location_top(result)
                height=dirc.get_words_result_location_height(result)
                left=dirc.get_words_result_location_left(result)
                div=html_writer.add_div(words,width,top,height,left,path)
                frame=frame+div
            frame=frame+end
            with open(path+".html","wt") as f_html:
                f_html.write(frame)
            print("\r完成第{}页。".format(n),end="")
            self.size=self.ocr._img_size(path)
            return path+".html"       

            
    def _creat_path(self):
        path=r"./{}".format(self.title)
        isExists=os.path.exists(path)
        if not isExists:
            os.makedirs(path)
        else:
            pass
       
    
    def _pdf_DownLoader(self):
        self._goto_iframe()
        self._read_all()
        all_page=self._get_all_page()
        img_link_list=[]
        for page in all_page:
            link=self._get_one_page(page)
            if link==None:
                pass
            else:
                img_link_list.append(link)
        i=1
        #print(img_link_list)
        html="<html><head></head><body>"
        for img_link in img_link_list:
            #print(img_link)
            #self._img_downloader(img_link,i)
            f_html=self._img_downloader(img_link,i)
            html=html+"""<iframe width="{}" height="{}" frameborder="0" scrolling="no" src="{}"></iframe>""".format(self.size[0],self.size[1],f_html)
            i+=1
        html=html+"</body></html>"
        with open("{}.html".format(self.title),"wt") as file:
            file.write(html)

    def quit(self):
        self.browser.quit()
        print("完成")

if __name__=="__main__":
    demo=MySpider(driverhead="--headless")
    demo._pdf_DownLoader()
    demo.quit()

