# book118网站pdf下载器

#### 介绍
可以下载book118网站pdf文件，生成html文件用浏览器打开。需要用到百度AI文字识别服务，需要自行申请百度文字识别服务的AK和SK。申请方式链接 https://ai.baidu.com/docs#/Begin/top

#### 原理
目标网站的PDF文件其实是把PDF的每页变成一张图片显示。使用百度AI的目的是将识别图片中的文字以及文字的位置信息。将百度AI识别的结果和图片封装成网页，使用浏览器查看需要下载的内容。


#### Python版本

Python 3.6

#### 运行环境
chrome
chromedriver(加入path)


#### 第三方库

1. selenium
2. requests
3. base64
4. Pillow


#### 使用说明

1. 获得AK和SK
2. 下载chromedriver.exe（版本需要和chrome版本一致），加入PATH或者，把chromedriver.exe放到和book118_spider.py相同目录中
3. 运行book118_spider.py，输入需要下载文件的网址。
4. 下载完成后，打开需要下载文件名字相同的html文件即可


#### 目前存在的问题

1. 有时候最后文件的最后几页不能下载，问题原因是含有目标文件链接的元素没有加载出来，需要设置等待，目前的等待方法似乎无效。
2. 百度AI每月可免费使用500页，不过500页一般够一个人一个月的使用了。
